﻿namespace Repository
{
    using DataTransferObjects;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="IRepository" />.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteCustomer(Int32 id);

        /// <summary>
        /// Deposits the funds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="funds">The funds.</param>
        void DepositFunds(Int32 customerId,
                          Decimal funds);

        /// <summary>
        /// Transfer funds.
        /// </summary>
        /// <param name="fromCustomer">Customer to get money from.</param>
        /// <param name="toCustomer">Customer to receive the money.</param>
        /// <param name="funds">Amount.</param>
        void TransferFunds(int fromCustomer, int toCustomer, decimal funds);

        /// <summary>
        /// Gets the available funds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>.</returns>
        Decimal GetAvailableFunds(Int32 customerId);

        /// <summary>
        /// Gets the customer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>.</returns>
        Customer GetCustomer(Int32 id);

        /// <summary>
        /// Get all available customers.
        /// </summary>
        /// <returns>.</returns>
        List<Customer> GetAllCustomers();

        /// <summary>
        /// Saves the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        void SaveCustomerAsync(Customer customer);

        /// <summary>
        /// Withdraws the funds.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="funds">The funds.</param>
        void WithdrawFunds(Int32 customerId,
                           Decimal funds);
    }
}