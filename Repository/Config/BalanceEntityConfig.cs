﻿using DataTransferObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repository.Config
{
    /// <summary>
    /// Defines the <see cref="BalanceEntityConfig" />.
    /// </summary>
    public class BalanceEntityConfig : IEntityTypeConfiguration<Balance>
    {
        /// <summary>
        /// The Configure.
        /// </summary>
        /// <param name="builder">The builder<see cref="EntityTypeBuilder{Balance}"/>.</param>
        public void Configure(EntityTypeBuilder<Balance> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Customer)
                .WithMany(x => x.Balances)
                .HasForeignKey(x => x.CustomerId);
        }
    }
}