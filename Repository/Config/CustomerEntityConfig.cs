﻿using DataTransferObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repository.Config
{
    /// <summary>
    /// Defines the <see cref="CustomerEntityConfig" />.
    /// </summary>
    public class CustomerEntityConfig : IEntityTypeConfiguration<Customer>
    {
        /// <summary>
        /// The Configure.
        /// </summary>
        /// <param name="builder">The builder<see cref="EntityTypeBuilder{Customer}"/>.</param>
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}