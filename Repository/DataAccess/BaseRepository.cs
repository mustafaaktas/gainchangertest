﻿using DataTransferObjects.Base;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Repository.DataAccess
{
    /// <summary>
    /// Defines the <see cref="BaseRepository{T}" />.
    /// </summary>
    /// <typeparam name="T">.</typeparam>
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        /// <summary>
        /// Defines the _dbFactory.
        /// </summary>
        private readonly DbFactory _dbFactory;

        /// <summary>
        /// Defines the _dbSet.
        /// </summary>
        private DbSet<T> _dbSet;

        /// <summary>
        /// Gets the DbSet.
        /// </summary>
        protected DbSet<T> DbSet { get => _dbSet ?? (_dbSet = _dbFactory.DbContext.Set<T>()); }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository{T}"/> class.
        /// </summary>
        /// <param name="dbFactory">The dbFactory<see cref="DbFactory"/>.</param>
        public BaseRepository(DbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        /// <summary>
        /// The Add.
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/>.</param>
        public void Add(T entity)
        {
            if (typeof(IAuditEntity).IsAssignableFrom(typeof(T)))
            {
                ((IAuditEntity)entity).CreatedDate = DateTime.UtcNow;
            }

            if (typeof(IDeleteEntity).IsAssignableFrom(typeof(T)))
            {
                ((IDeleteEntity)entity).IsDeleted = false;
            }

            DbSet.Add(entity);
        }

        /// <summary>
        /// The Delete.
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/>.</param>
        public void Delete(T entity)
        {
            if (typeof(IDeleteEntity).IsAssignableFrom(typeof(T)))
            {
                ((IDeleteEntity)entity).IsDeleted = true;
                DbSet.Update(entity);
            }
            else
                DbSet.Remove(entity);
        }

        /// <summary>
        /// The List.
        /// </summary>
        /// <param name="expression">The expression<see cref="Expression{Func{T, bool}}"/>.</param>
        /// <returns>The <see cref="IQueryable{T}"/>.</returns>
        public IQueryable<T> List(Expression<Func<T, bool>> expression)
        {
            return DbSet.Where(expression);
        }

        /// <summary>
        /// The Update.
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/>.</param>
        public void Update(T entity)
        {
            if (typeof(IAuditEntity).IsAssignableFrom(typeof(T)))
            {
                ((IAuditEntity)entity).UpdatedDate = DateTime.UtcNow;
            }
            DbSet.Update(entity);
        }
    }
}