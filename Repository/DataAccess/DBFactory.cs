﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Repository.DataAccess
{
    /// <summary>
    /// Defines the <see cref="DbFactory" />.
    /// </summary>
    public class DbFactory : IDisposable
    {
        /// <summary>
        /// Defines the _disposed.
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Defines the _instanceFunc.
        /// </summary>
        private Func<MainDbContext> _instanceFunc;

        /// <summary>
        /// Defines the _dbContext.
        /// </summary>
        private DbContext _dbContext;

        /// <summary>
        /// Gets the DbContext.
        /// </summary>
        public DbContext DbContext => _dbContext ?? (_dbContext = _instanceFunc.Invoke());

        /// <summary>
        /// Initializes a new instance of the <see cref="DbFactory"/> class.
        /// </summary>
        /// <param name="dbContextFactory">The dbContextFactory<see cref="Func{MainDbContext}"/>.</param>
        public DbFactory(Func<MainDbContext> dbContextFactory)
        {
            _instanceFunc = dbContextFactory;
        }

        /// <summary>
        /// The Dispose.
        /// </summary>
        public void Dispose()
        {
            if (!_disposed && _dbContext != null)
            {
                _disposed = true;
                _dbContext.Dispose();
            }
        }
    }
}