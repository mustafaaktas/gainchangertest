﻿using DataTransferObjects;
using Repository.Interfaces;

namespace Repository.DataAccess
{
    /// <summary>
    /// Defines the <see cref="CustomerRepository" />.
    /// </summary>
    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="dbFactory">The dbFactory<see cref="DbFactory"/>.</param>
        public CustomerRepository(DbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}