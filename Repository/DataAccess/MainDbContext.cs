﻿using DataTransferObjects;
using Microsoft.EntityFrameworkCore;
using Repository.Config;

namespace Repository.DataAccess
{
    /// <summary>
    /// Defines the <see cref="MainDbContext" />.
    /// </summary>
    public class MainDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainDbContext"/> class.
        /// </summary>
        /// <param name="options">The options<see cref="DbContextOptions{MainDbContext}"/>.</param>
        public MainDbContext(DbContextOptions<MainDbContext> options)
        : base(options)
        {
        }

        /// <summary>
        /// Gets or sets the Customers.
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Gets or sets the Balances.
        /// </summary>
        public DbSet<Balance> Balances { get; set; }

        /// <summary>
        /// The OnModelCreating.
        /// </summary>
        /// <param name="modelBuilder">The modelBuilder<see cref="ModelBuilder"/>.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            ApplyConfigurations(modelBuilder);
        }

        /// <summary>
        /// The ApplyConfigurations.
        /// </summary>
        /// <param name="modelBuilder">The modelBuilder<see cref="ModelBuilder"/>.</param>
        private static void ApplyConfigurations(ModelBuilder modelBuilder) => modelBuilder
                .ApplyConfiguration(new CustomerEntityConfig())
                .ApplyConfiguration(new BalanceEntityConfig());
    }
}