﻿using Repository.Interfaces;
using System.Threading.Tasks;

namespace Repository.DataAccess
{
    /// <summary>
    /// Defines the <see cref="UnitOfWork" />.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Defines the _dbFactory.
        /// </summary>
        private DbFactory _dbFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="dbFactory">The dbFactory<see cref="DbFactory"/>.</param>
        public UnitOfWork(DbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        /// <summary>
        /// The SaveChangesAsync.
        /// </summary>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        public Task<int> SaveChangesAsync()
        {
            return _dbFactory.DbContext.SaveChangesAsync();
        }
    }
}