﻿namespace Repository.DataAccess
{
    /// <summary>
    /// Defines the <see cref="DbInitializer" />.
    /// </summary>
    public class DbInitializer
    {
        /// <summary>
        /// The Initialize.
        /// </summary>
        /// <param name="context">The context<see cref="MainDbContext"/>.</param>
        public static void Initialize(MainDbContext context)
        {
            context.Database.EnsureCreated();
            context.SaveChanges();
        }
    }
}