﻿using DataTransferObjects;
using Repository.Interfaces;

namespace Repository.DataAccess
{
    /// <summary>
    /// Defines the <see cref="BalanceRepository" />.
    /// </summary>
    public class BalanceRepository : BaseRepository<Balance>, IBalanceRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BalanceRepository"/> class.
        /// </summary>
        /// <param name="dbFactory">The dbFactory<see cref="DbFactory"/>.</param>
        public BalanceRepository(DbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}