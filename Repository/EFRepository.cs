﻿using DataTransferObjects;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    /// <summary>
    /// Defines the <see cref="EFRepository" />.
    /// </summary>
    public class EFRepository : IRepository
    {
        /// <summary>
        /// Defines the _customerRepo.
        /// </summary>
        private readonly ICustomerRepository _customerRepo;

        /// <summary>
        /// Defines the _balanceRepo.
        /// </summary>
        private readonly IBalanceRepository _balanceRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="EFRepository"/> class.
        /// </summary>
        /// <param name="customerRepo">The customerRepo<see cref="ICustomerRepository"/>.</param>
        /// <param name="balanceRepo">The balanceRepo<see cref="IBalanceRepository"/>.</param>
        public EFRepository(ICustomerRepository customerRepo, IBalanceRepository balanceRepo)
        {
            _balanceRepo = balanceRepo;
            _customerRepo = customerRepo;
        }

        /// <summary>
        /// The DeleteCustomer.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        public void DeleteCustomer(int id)
        {
            var existing = _customerRepo.List(x => x.Id == id);
            if (existing.Count() == 0)
                throw new ArgumentException("Invalid Customer");

            _customerRepo.Delete(existing.First());
        }

        /// <summary>
        /// The DepositFunds.
        /// </summary>
        /// <param name="customerId">The customerId<see cref="int"/>.</param>
        /// <param name="funds">The funds<see cref="decimal"/>.</param>
        public void DepositFunds(int customerId, decimal funds)
        {
            checkCustomerAvailability(customerId);
            var balances = _balanceRepo.List(x => x.CustomerId == customerId);
            var lastBalance = balances.Count() > 0 ? balances.OrderByDescending(x => x.CreatedDate).First() : null;

            _balanceRepo.Add(new Balance
            {
                CustomerId = customerId,
                CreatedBy = "staticUser",
                CreatedDate = DateTime.UtcNow,
                Value = lastBalance == null ? funds : lastBalance.Value + funds
            });
        }

        /// <summary>
        /// The GetAllCustomers.
        /// </summary>
        /// <returns>The <see cref="List{Customer}"/>.</returns>
        public List<Customer> GetAllCustomers()
        {
            return _customerRepo.List(x => x.IsDeleted == false).ToListAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// The GetAvailableFunds.
        /// </summary>
        /// <param name="customerId">The customerId<see cref="int"/>.</param>
        /// <returns>The <see cref="decimal"/>.</returns>
        public decimal GetAvailableFunds(int customerId)
        {
            checkCustomerAvailability(customerId);

            var balances = _balanceRepo.List(x => x.CustomerId == customerId);
            var lastBalance = balances.Count() > 0 ? balances.OrderByDescending(x => x.CreatedDate).First() : null;
            return lastBalance == null ? 0 : lastBalance.Value;
        }

        /// <summary>
        /// The checkCustomerAvailability.
        /// </summary>
        /// <param name="customerId">The customerId<see cref="int"/>.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        private bool checkCustomerAvailability(int customerId)
        {
            var list = _customerRepo.List(x => x.Id == customerId);
            var customer = list.Count() > 0 ? list.First() : null;

            if (customer == null || customer.IsDeleted == true)
                throw new ArgumentException("This customer doesn't exist or they can't be used for transactions");

            return true;
        }

        /// <summary>
        /// The GetCustomer.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <returns>The <see cref="Customer"/>.</returns>
        public Customer GetCustomer(int id)
        {
            var existing = _customerRepo.List(x => x.Id == id);
            return existing.Count() > 0 ? existing.First() : null;
        }

        /// <summary>
        /// The SaveCustomerAsync.
        /// </summary>
        /// <param name="customer">The customer<see cref="Customer"/>.</param>
        public void SaveCustomerAsync(Customer customer)
        {
            var existing = _customerRepo.List(x => x.Id == customer.Id);
            if (existing.Count() > 0)
            {
                var updated = existing.First();
                updated.IdCard = customer.IdCard;
                updated.Name = customer.Name;
                updated.Surname = customer.Surname;
                _customerRepo.Update(updated);
            }
            else
            {
                _customerRepo.Add(customer);
            }
        }

        /// <summary>
        /// The WithdrawFunds.
        /// </summary>
        /// <param name="customerId">The customerId<see cref="int"/>.</param>
        /// <param name="funds">The funds<see cref="decimal"/>.</param>
        public void WithdrawFunds(int customerId, decimal funds)
        {
            checkCustomerAvailability(customerId);
            var balances = _balanceRepo.List(x => x.CustomerId == customerId);
            var lastBalance = balances.Count() > 0 ? balances.OrderByDescending(x => x.CreatedDate).First() : null;
            if (lastBalance == null)
                throw new ArgumentException("No balance found");
            else if ((lastBalance.Value - funds) < 0)
                throw new ArgumentException("You can only withdraw " + lastBalance.Value.ToString());

            _balanceRepo.Add(new Balance
            {
                CustomerId = customerId,
                CreatedBy = "staticUser",
                CreatedDate = DateTime.UtcNow,
                Value = lastBalance.Value - funds
            });
        }

        /// <summary>
        /// The TransferFunds.
        /// </summary>
        /// <param name="fromCustomer">The fromCustomer<see cref="int"/>.</param>
        /// <param name="toCustomer">The toCustomer<see cref="int"/>.</param>
        /// <param name="funds">The funds<see cref="decimal"/>.</param>
        public void TransferFunds(int fromCustomer, int toCustomer, decimal funds)
        {
            this.WithdrawFunds(fromCustomer, funds);
            this.DepositFunds(toCustomer, funds);
        }
    }
}