﻿using System.Threading.Tasks;

namespace Repository.Interfaces
{
    /// <summary>
    /// Defines the <see cref="IUnitOfWork" />.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// The SaveChangesAsync.
        /// </summary>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        Task<int> SaveChangesAsync();
    }
}