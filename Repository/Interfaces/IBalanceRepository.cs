﻿using DataTransferObjects;

namespace Repository.Interfaces
{
    /// <summary>
    /// Defines the <see cref="IBalanceRepository" />.
    /// </summary>
    public interface IBalanceRepository : IBaseRepository<Balance>
    {
    }
}