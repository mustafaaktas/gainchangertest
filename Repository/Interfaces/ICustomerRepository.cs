﻿using DataTransferObjects;

namespace Repository.Interfaces
{
    /// <summary>
    /// Defines the <see cref="ICustomerRepository" />.
    /// </summary>
    public interface ICustomerRepository : IBaseRepository<Customer>
    {
    }
}