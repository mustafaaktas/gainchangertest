﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Repository.Interfaces
{
    /// <summary>
    /// Defines the <see cref="IBaseRepository{T}" />.
    /// </summary>
    /// <typeparam name="T">.</typeparam>
    public interface IBaseRepository<T> where T : class
    {
        /// <summary>
        /// The Add.
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/>.</param>
        void Add(T entity);

        /// <summary>
        /// The Delete.
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/>.</param>
        void Delete(T entity);

        /// <summary>
        /// The Update.
        /// </summary>
        /// <param name="entity">The entity<see cref="T"/>.</param>
        void Update(T entity);

        /// <summary>
        /// The List.
        /// </summary>
        /// <param name="expression">The expression<see cref="Expression{Func{T, bool}}"/>.</param>
        /// <returns>The <see cref="IQueryable{T}"/>.</returns>
        IQueryable<T> List(Expression<Func<T, bool>> expression);
    }
}