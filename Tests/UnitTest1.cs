using BusinessLogic;
using DataTransferObjects;
using Moq;
using Repository;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Tests
{
    public class UnitTest1
    {
        [InlineData(1, 2, true)]
        [InlineData(2, 1, false)]
        [Theory]
        public void TestTransfer(int from, int to, bool expected)
        {
            var res = true;
            Customer customer1 = new Customer
            {
                Id = 1,
                Name = "test1",
                Surname = "test1",
                IdCard = "test1",
                Balances = new System.Collections.Generic.List<Balance>
                {
                    new Balance
                    {
                        CreatedDate = DateTime.UtcNow,
                        Value = 50
                    }
                }
            };
            Customer customer2 = new Customer
            {
                Id = 2,
                Name = "test2",
                Surname = "test2",
                IdCard = "test2",
                Balances = new System.Collections.Generic.List<Balance>
                {
                    new Balance
                    {
                        CreatedDate = DateTime.UtcNow,
                        Value = 0
                    }
                }
            };
            var customerRepo = new Mock<ICustomerRepository>();
            var balanceRepo = new Mock<IBalanceRepository>();
            customerRepo.Setup(x => x.List(x => x.Id == 1)).Returns(new List<Customer> { customer1 }.AsQueryable());
            customerRepo.Setup(x => x.List(x => x.Id == 2)).Returns(new List<Customer> { customer2 }.AsQueryable());
            balanceRepo.Setup(x => x.List(x => x.CustomerId == 1)).Returns(customer1.Balances.AsQueryable());
            balanceRepo.Setup(x => x.List(x => x.CustomerId == 2)).Returns(customer2.Balances.AsQueryable());
            var repo = new EFRepository(customerRepo.Object, balanceRepo.Object);
            var unit = new Mock<IUnitOfWork>();
            var manager = new AccountsManager(repo, unit.Object);

            try
            {
                manager.TransferFunds(from, to, 20);
            }
            catch
            {
                res = false;
            }
            Assert.Equal(res, expected);
        }
    }
}