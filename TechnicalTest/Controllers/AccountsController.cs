﻿using BusinessLogic;
using Microsoft.AspNetCore.Mvc;

namespace TechnicalTest.Controllers
{
    /// <summary>
    /// Defines the <see cref="AccountsController" />.
    /// </summary>
    [Route("api/accounts")]
    [ApiController]
    public class AccountsController : Controller
    {
        /// <summary>
        /// Defines the AccountsManager.
        /// </summary>
        private readonly IAccountsManager AccountsManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsController"/> class.
        /// </summary>
        /// <param name="accountsManager">The accountsManager<see cref="IAccountsManager"/>.</param>
        public AccountsController(IAccountsManager accountsManager)
        {
            AccountsManager = accountsManager;
        }

        /// <summary>
        /// The Deposit.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="command">The command<see cref="AccountCommand"/>.</param>
        [HttpPost("/{id}/deposit")]
        public void Deposit(int id, [FromBody] AccountCommand command)
        {
            this.AccountsManager.DepositFunds(id, command.Funds);
        }

        /// <summary>
        /// The Withdraw.
        /// </summary>
        /// <param name="id">The id<see cref="int"/>.</param>
        /// <param name="command">The command<see cref="AccountCommand"/>.</param>
        [HttpPost("/{id}/withdraw")]
        public void Withdraw(int id, [FromBody] AccountCommand command)
        {
            this.AccountsManager.WithdrawFunds(id, command.Funds);
        }

        /// <summary>
        /// The Transfer.
        /// </summary>
        /// <param name="command">The command<see cref="TransferCommand"/>.</param>
        [HttpPost("/transfer")]
        public void Transfer([FromBody] TransferCommand command)
        {
            this.AccountsManager.TransferFunds(command.From, command.To, command.Funds);
        }
    }
}