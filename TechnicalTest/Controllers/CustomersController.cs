﻿namespace TechnicalTest.Controllers
{
    using BusinessLogic;
    using DataTransferObjects;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="CustomersController" />.
    /// </summary>
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        /// <summary>
        /// The customers manager.
        /// </summary>
        private readonly ICustomersManager CustomersManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersController"/> class.
        /// </summary>
        /// <param name="customersManager">The customers manager.</param>
        public CustomersController(ICustomersManager customersManager)
        {
            this.CustomersManager = customersManager;
        }

        // DELETE api/customers/5
        /// <summary>
        /// The Delete.
        /// </summary>
        /// <param name="id">The id<see cref="Int32"/>.</param>
        [HttpDelete("{id}")]
        public void Delete(Int32 id)
        {
            this.CustomersManager.DeleteCustomer(id);
        }

        // GET api/customers
        /// <summary>
        /// The Get.
        /// </summary>
        /// <returns>The <see cref="ActionResult{IEnumerable{Customer}}"/>.</returns>
        [HttpGet]
        public ActionResult<IEnumerable<Customer>> Get()
        {
            return this.Ok(this.CustomersManager.GetAllCustomers());
        }

        // GET api/customers/5
        /// <summary>
        /// The Get.
        /// </summary>
        /// <param name="id">The id<see cref="Int32"/>.</param>
        /// <returns>The <see cref="ActionResult{String}"/>.</returns>
        [HttpGet("{id}")]
        public ActionResult<String> Get(Int32 id)
        {
            Customer customer = this.CustomersManager.GetCustomer(id);
            return this.Ok(customer);
        }

        // POST api/customers
        /// <summary>
        /// The Post.
        /// </summary>
        /// <param name="customer">The customer<see cref="Customer"/>.</param>
        [HttpPost]
        public void Post([FromBody] Customer customer)
        {
            this.CustomersManager.SaveCustomer(customer);
        }

        // PUT api/customers/5
        /// <summary>
        /// The Put.
        /// </summary>
        /// <param name="customer">The customer<see cref="Customer"/>.</param>
        [HttpPut("{id}")]
        public void Put([FromBody] Customer customer)
        {
            this.CustomersManager.SaveCustomer(customer);
        }
    }
}