﻿using Repository;
using Repository.Interfaces;

namespace BusinessLogic
{
    /// <summary>
    /// Defines the <see cref="AccountsManager" />.
    /// </summary>
    public class AccountsManager : IAccountsManager
    {
        /// <summary>
        /// Defines the UnitOfWork.
        /// </summary>
        private readonly IUnitOfWork UnitOfWork;

        /// <summary>
        /// Defines the Repository.
        /// </summary>
        private readonly IRepository Repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsManager"/> class.
        /// </summary>
        /// <param name="repository">The repository<see cref="IRepository"/>.</param>
        /// <param name="unitOfWork">The unitOfWork<see cref="IUnitOfWork"/>.</param>
        public AccountsManager(IRepository repository, IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
            Repository = repository;
        }

        /// <summary>
        /// The DepositFunds.
        /// </summary>
        /// <param name="customerId">The customerId<see cref="int"/>.</param>
        /// <param name="funds">The funds<see cref="decimal"/>.</param>
        public void DepositFunds(int customerId, decimal funds)
        {
            this.Repository.DepositFunds(customerId, funds);
            this.UnitOfWork.SaveChangesAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// The TransferFunds.
        /// </summary>
        /// <param name="fromCustomer">The fromCustomer<see cref="int"/>.</param>
        /// <param name="toCustomer">The toCustomer<see cref="int"/>.</param>
        /// <param name="funds">The funds<see cref="decimal"/>.</param>
        public void TransferFunds(int fromCustomer, int toCustomer, decimal funds)
        {
            this.Repository.TransferFunds(fromCustomer, toCustomer, funds);
            this.UnitOfWork.SaveChangesAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// The WithdrawFunds.
        /// </summary>
        /// <param name="customerId">The customerId<see cref="int"/>.</param>
        /// <param name="funds">The funds<see cref="decimal"/>.</param>
        public void WithdrawFunds(int customerId, decimal funds)
        {
            this.Repository.WithdrawFunds(customerId, funds);
            this.UnitOfWork.SaveChangesAsync().GetAwaiter().GetResult();
        }
    }
}