﻿namespace BusinessLogic
{
    using DataTransferObjects;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="ICustomersManager" />.
    /// </summary>
    public interface ICustomersManager
    {
        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteCustomer(Int32 id);

        /// <summary>
        /// Gets the customer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>.</returns>
        Customer GetCustomer(Int32 id);

        /// <summary>
        /// Adds the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        void SaveCustomer(Customer customer);

        /// <summary>
        /// The GetAllCustomers.
        /// </summary>
        /// <returns>The <see cref="List{Customer}"/>.</returns>
        List<Customer> GetAllCustomers();
    }
}