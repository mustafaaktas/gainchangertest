﻿namespace BusinessLogic
{
    /// <summary>
    /// Defines the <see cref="AccountCommand" />.
    /// </summary>
    public class AccountCommand
    {
        /// <summary>
        /// Gets or sets the Funds.
        /// </summary>
        public decimal Funds { get; set; }
    }
}