﻿namespace BusinessLogic
{
    /// <summary>
    /// Defines the <see cref="TransferCommand" />.
    /// </summary>
    public class TransferCommand : AccountCommand
    {
        /// <summary>
        /// Gets or sets the From.
        /// </summary>
        public int From { get; set; }

        /// <summary>
        /// Gets or sets the To.
        /// </summary>
        public int To { get; set; }
    }
}