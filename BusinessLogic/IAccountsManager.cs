﻿namespace BusinessLogic
{
    /// <summary>
    /// Defines the <see cref="IAccountsManager" />.
    /// </summary>
    public interface IAccountsManager
    {
        /// <summary>
        /// The DepositFunds.
        /// </summary>
        /// <param name="customerId">The customerId<see cref="int"/>.</param>
        /// <param name="funds">The funds<see cref="decimal"/>.</param>
        void DepositFunds(int customerId, decimal funds);

        /// <summary>
        /// The WithdrawFunds.
        /// </summary>
        /// <param name="customerId">The customerId<see cref="int"/>.</param>
        /// <param name="funds">The funds<see cref="decimal"/>.</param>
        void WithdrawFunds(int customerId, decimal funds);

        /// <summary>
        /// The TransferFunds.
        /// </summary>
        /// <param name="fromCustomer">The fromCustomer<see cref="int"/>.</param>
        /// <param name="toCustomer">The toCustomer<see cref="int"/>.</param>
        /// <param name="funds">The funds<see cref="decimal"/>.</param>
        void TransferFunds(int fromCustomer, int toCustomer, decimal funds);
    }
}