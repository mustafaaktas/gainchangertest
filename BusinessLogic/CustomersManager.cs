﻿namespace BusinessLogic
{
    using DataTransferObjects;
    using Repository;
    using Repository.Interfaces;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="CustomersManager" />.
    /// </summary>
    public class CustomersManager : ICustomersManager
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IRepository Repository;

        /// <summary>
        /// Defines the UnitOfWork.
        /// </summary>
        private readonly IUnitOfWork UnitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersManager"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="unitOfWork">The unitOfWork<see cref="IUnitOfWork"/>.</param>
        public CustomersManager(IRepository repository, IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
            this.Repository = repository;
        }

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void DeleteCustomer(Int32 id)
        {
            this.Repository.DeleteCustomer(id);
            this.UnitOfWork.SaveChangesAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// The GetAllCustomers.
        /// </summary>
        /// <returns>The <see cref="List{Customer}"/>.</returns>
        public List<Customer> GetAllCustomers()
        {
            return this.Repository.GetAllCustomers();
        }

        /// <summary>
        /// Gets the customer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>.</returns>
        public Customer GetCustomer(Int32 id)
        {
            return this.Repository.GetCustomer(id);
        }

        /// <summary>
        /// Adds the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        public void SaveCustomer(Customer customer)
        {
            this.Repository.SaveCustomerAsync(customer);
            this.UnitOfWork.SaveChangesAsync().GetAwaiter().GetResult();
        }
    }
}