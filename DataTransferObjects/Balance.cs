﻿using DataTransferObjects.Base;

namespace DataTransferObjects
{
    /// <summary>
    /// Defines the <see cref="Balance" />.
    /// </summary>
    public class Balance : AuditEntity<int>
    {
        /// <summary>
        /// Gets or sets the CustomerId.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the Customer.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets the Value.
        /// </summary>
        public decimal Value { get; set; }
    }
}