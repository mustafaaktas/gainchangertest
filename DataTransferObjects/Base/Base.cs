﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataTransferObjects.Base
{
    /// <summary>
    /// Defines the <see cref="IEntityBase{TKey}" />.
    /// </summary>
    /// <typeparam name="TKey">.</typeparam>
    public interface IEntityBase<TKey>
    {
        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        TKey Id { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="IDeleteEntity" />.
    /// </summary>
    public interface IDeleteEntity
    {
        /// <summary>
        /// Gets or sets a value indicating whether IsDeleted.
        /// </summary>
        bool IsDeleted { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="IDeleteEntity{TKey}" />.
    /// </summary>
    /// <typeparam name="TKey">.</typeparam>
    public interface IDeleteEntity<TKey> : IDeleteEntity, IEntityBase<TKey>
    {
    }

    /// <summary>
    /// Defines the <see cref="IAuditEntity" />.
    /// </summary>
    public interface IAuditEntity
    {
        /// <summary>
        /// Gets or sets the CreatedDate.
        /// </summary>
        DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy.
        /// </summary>
        string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the UpdatedDate.
        /// </summary>
        DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Gets or sets the UpdatedBy.
        /// </summary>
        string UpdatedBy { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="IAuditEntity{TKey}" />.
    /// </summary>
    /// <typeparam name="TKey">.</typeparam>
    public interface IAuditEntity<TKey> : IAuditEntity, IDeleteEntity<TKey>
    {
    }

    /// <summary>
    /// Defines the <see cref="EntityBase{TKey}" />.
    /// </summary>
    /// <typeparam name="TKey">.</typeparam>
    public abstract class EntityBase<TKey> : IEntityBase<TKey>
    {
        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual TKey Id { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="DeleteEntity{TKey}" />.
    /// </summary>
    /// <typeparam name="TKey">.</typeparam>
    public abstract class DeleteEntity<TKey> : EntityBase<TKey>, IDeleteEntity<TKey>
    {
        /// <summary>
        /// Gets or sets a value indicating whether IsDeleted.
        /// </summary>
        public bool IsDeleted { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="AuditEntity{TKey}" />.
    /// </summary>
    /// <typeparam name="TKey">.</typeparam>
    public abstract class AuditEntity<TKey> : DeleteEntity<TKey>, IAuditEntity<TKey>
    {
        /// <summary>
        /// Gets or sets the CreatedDate.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy.
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the UpdatedDate.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Gets or sets the UpdatedBy.
        /// </summary>
        public string UpdatedBy { get; set; }
    }
}