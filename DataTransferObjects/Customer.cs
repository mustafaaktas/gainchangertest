﻿namespace DataTransferObjects
{
    using DataTransferObjects.Base;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="Customer" />.
    /// </summary>
    public class Customer : DeleteEntity<int>
    {
        /// <summary>
        /// Gets or sets the identifier card..
        /// </summary>
        public String IdCard { get; set; }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Gets or sets the Surname.
        /// </summary>
        public String Surname { get; set; }

        /// <summary>
        /// Gets or sets the Balances.
        /// </summary>
        public virtual List<Balance> Balances { get; set; }
    }
}